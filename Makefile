all: build test

build:
	go build -o echoserver echo.go

test:
	go test -v

clean:
	rm -f echoserver

run:
	go run echo.go
