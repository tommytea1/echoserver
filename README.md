# echoserver
HTTP echo service which echoes back POST/POST requests in JSON, appending ```"echoed":true```, unless field is already present and boolean true in the request.

## Tradeoffs / TODOs
* Account Management
* TLS / 2-way TLS
* Consistent units of time measure (ms)
* Separate log files
* System resource usage metrics
* Load testing
* More tests
* Profiling
* Minimize/Slim down the container

## Dependencies
* git
* golang

## Download
```
git clone https://gitlab.com/tommytea1/echoserver.git
```

## Build
```
cd echoserver
go build -o echoserver echo.go
```

## Run
```
./echoserver
```

## Test
```
go test -v

OR

curl -v --header "Content-Type: application/json" --request POST --data '{"username":"xyz","upload":"xyz"}' http://localhost:3000/api/echo

curl -v --header "Content-Type: application/json" --request PUT --data '{"username":"xyz","upload":"xyz"}' http://localhost:3000/api/echo
```

## Docker
```
cd echoserver
sudo docker build -t echoserver .
sudo docker run --rm -p 3000:3000 echoserver:latest
```
