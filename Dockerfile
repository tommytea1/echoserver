FROM golang:alpine3.14@sha256:a8df40ad1380687038af912378f91cf26aeabb05046875df0bfedd38a79b5499

RUN apk update && apk upgrade && apk add --no-cache git \
  && git clone https://gitlab.com/tommytea1/echoserver.git \
  && cd echoserver && go build -o echoserver echo.go

EXPOSE 8080/tcp

CMD ["/go/echoserver/echoserver"]