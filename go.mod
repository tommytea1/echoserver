module echo

go 1.16

require (
	github.com/gofiber/fiber/v2 v2.15.0
	github.com/gofiber/utils v0.1.2
)
