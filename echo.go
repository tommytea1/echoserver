package main

import (
	"os"
	"log"
	"fmt"
	"time"

	json "encoding/json"
	fiber "github.com/gofiber/fiber/v2"
)

var (
	DebugLogger *log.Logger
	InfoLogger *log.Logger
	WarnLogger *log.Logger
	ErrorLogger *log.Logger
)

func echo(ctx *fiber.Ctx) error {
	// Start echo function timer for metrics
	echoStart := time.Now()
	defer func() {
		DebugLogger.Printf("Echo for %s took %s", ctx.IP(), time.Since(echoStart))
	}()

	var requestBody map[string]interface{}

	// Leverage encoding/json to perform JSON sanity check + deserialization
	if err := json.Unmarshal(ctx.Body(), &requestBody); err != nil {
		// Respond with 400 Bad Request if malformed JSON
		WarnLogger.Println(fmt.Sprintf("400 Bad Request (malformed json) from %s", ctx.IP()))
		return ctx.SendStatus(400)
	}

	// Check if "echoed" field exists in request
	if echoed, exists := requestBody["echoed"]; exists {
		// Check if "echoed" field is boolean true
		if bEchoed, isBool := echoed.(bool); isBool {
			if bEchoed {
				// Respond with 400 Bad Request per requirement if "echoed" field exists is and boolean true
				WarnLogger.Println(fmt.Sprintf("400 Bad Request from %s", ctx.IP()))
				return ctx.SendStatus(400)
			}
		}
	}

	// Otherwise set "echoed" field to boolean true
	requestBody["echoed"] = true

	// Serialize JSON response
	responseBody, err := json.Marshal(requestBody)
	if err != nil {
		// Respond with 500 Internal Server Error if some error arose when serializing JSON response
		ErrorLogger.Println(fmt.Sprintf("500 Internal Server Error json.Marshal(%s)", requestBody))
		return ctx.SendStatus(500)
	}

	InfoLogger.Println(fmt.Sprintf("Response to %s: %s", ctx.IP(), responseBody))
	return ctx.Send(responseBody)
}

func main() {
	// Initialize logging
	fileLog, err := os.OpenFile("echo.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}
	DebugLogger = log.New(fileLog, "DEBUG: ", log.Ldate|log.Ltime|log.Lshortfile)
	InfoLogger = log.New(fileLog, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)
	WarnLogger = log.New(fileLog, "WARN: ", log.Ldate|log.Ltime|log.Lshortfile)
	ErrorLogger = log.New(fileLog, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)

	// Initialize Fiber Framework
	app := fiber.New()

	app.Post("/api/echo", echo)
	app.Put("/api/echo", echo)

	app.Listen(":3000")
}