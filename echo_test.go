package main

import (
	"os"
	"io"
	"log"
	"bytes"
	"testing"
	"net/http/httptest"

	fiber "github.com/gofiber/fiber/v2"
	utils "github.com/gofiber/utils"
)

func init() {
	// Initialize logging before tests
	fileLog, err := os.OpenFile("echo_test.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}
	DebugLogger = log.New(fileLog, "DEBUG: ", log.Ldate|log.Ltime|log.Lshortfile)
	InfoLogger = log.New(fileLog, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)
	WarnLogger = log.New(fileLog, "WARN: ", log.Ldate|log.Ltime|log.Lshortfile)
	ErrorLogger = log.New(fileLog, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)
}

// Test POST
func TestEcho_POST(t *testing.T) {
	app := fiber.New()
	app.Post("/api/echo", echo)

	reqBody := bytes.NewBuffer([]byte(`{"username":"xyz","upload":"xyz"}`))
	req := httptest.NewRequest("POST", "/api/echo", reqBody)
	res, err := app.Test(req)

	utils.AssertEqual(t, nil, err, "app.Test")
	utils.AssertEqual(t, 200, res.StatusCode, "HTTP Status Code")

	if res.StatusCode == 200 {
		resBody, _ := io.ReadAll(res.Body)
		utils.AssertEqual(t, `{"echoed":true,"upload":"xyz","username":"xyz"}`, string(resBody), "Response Body")
	}
}

// Test POST malformed JSON
func TestEcho_POST_malformed(t *testing.T) {
	app := fiber.New()
	app.Post("/api/echo", echo)

	reqBody := bytes.NewBuffer([]byte(`{"username":"xyz",upload":"xyz"}`))
	req := httptest.NewRequest("POST", "/api/echo", reqBody)
	res, err := app.Test(req)

	utils.AssertEqual(t, nil, err, "app.Test")
	utils.AssertEqual(t, 400, res.StatusCode, "HTTP Status Code")

	resBody, _ := io.ReadAll(res.Body)
	utils.AssertEqual(t, `Bad Request`, string(resBody), "Response Body")
}

// Test POST when "echoed" is true
func TestEcho_POST_echoed_true(t *testing.T) {
	app := fiber.New()
	app.Post("/api/echo", echo)

	reqBody := bytes.NewBuffer([]byte(`{"username":"xyz","upload":"xyz","echoed":true}`))
	req := httptest.NewRequest("POST", "/api/echo", reqBody)
	res, err := app.Test(req)

	utils.AssertEqual(t, nil, err, "app.Test")
	utils.AssertEqual(t, 400, res.StatusCode, "HTTP Status Code")

	resBody, _ := io.ReadAll(res.Body)
	utils.AssertEqual(t, `Bad Request`, string(resBody), "Response Body")
}

// Test POST when "echoed" is "true"
func TestEcho_POST_echoed_notsotrue(t *testing.T) {
	app := fiber.New()
	app.Post("/api/echo", echo)

	reqBody := bytes.NewBuffer([]byte(`{"username":"xyz","upload":"xyz","echoed":"true"}`))
	req := httptest.NewRequest("POST", "/api/echo", reqBody)
	res, err := app.Test(req)

	utils.AssertEqual(t, nil, err, "app.Test")
	utils.AssertEqual(t, 200, res.StatusCode, "HTTP Status Code")

	resBody, _ := io.ReadAll(res.Body)
	utils.AssertEqual(t, `{"echoed":true,"upload":"xyz","username":"xyz"}`, string(resBody), "Response Body")
}

// Test POST when "echoed" is false
func TestEcho_POST_echoed_false(t *testing.T) {
	app := fiber.New()
	app.Post("/api/echo", echo)

	reqBody := bytes.NewBuffer([]byte(`{"username":"xyz","upload":"xyz","echoed":false}`))
	req := httptest.NewRequest("POST", "/api/echo", reqBody)
	res, err := app.Test(req)

	utils.AssertEqual(t, nil, err, "app.Test")
	utils.AssertEqual(t, 200, res.StatusCode, "HTTP Status Code")

	resBody, _ := io.ReadAll(res.Body)
	utils.AssertEqual(t, `{"echoed":true,"upload":"xyz","username":"xyz"}`, string(resBody), "Response Body")
}

// Test POST when "echoed" is "false""
func TestEcho_POST_echoed_notsofalse(t *testing.T) {
	app := fiber.New()
	app.Post("/api/echo", echo)

	reqBody := bytes.NewBuffer([]byte(`{"username":"xyz","upload":"xyz","echoed":"false"}`))
	req := httptest.NewRequest("POST", "/api/echo", reqBody)
	res, err := app.Test(req)

	utils.AssertEqual(t, nil, err, "app.Test")
	utils.AssertEqual(t, 200, res.StatusCode, "HTTP Status Code")

	resBody, _ := io.ReadAll(res.Body)
	utils.AssertEqual(t, `{"echoed":true,"upload":"xyz","username":"xyz"}`, string(resBody), "Response Body")
}

// Test PUT
func TestEcho_PUT(t *testing.T) {
	app := fiber.New()
	app.Put("/api/echo", echo)

	reqBody := bytes.NewBuffer([]byte(`{"username":"xyz","upload":"xyz"}`))
	req := httptest.NewRequest("PUT", "/api/echo", reqBody)
	res, err := app.Test(req)

	utils.AssertEqual(t, nil, err, "app.Test")
	utils.AssertEqual(t, 200, res.StatusCode, "HTTP Status Code")

	if res.StatusCode == 200 {
		resBody, _ := io.ReadAll(res.Body)
		utils.AssertEqual(t, `{"echoed":true,"upload":"xyz","username":"xyz"}`, string(resBody), "Response Body")
	}
}

// Test PUT malformed JSON
func TestEcho_PUT_malformed(t *testing.T) {
	app := fiber.New()
	app.Put("/api/echo", echo)

	reqBody := bytes.NewBuffer([]byte(`{"username":"xyz",upload":"xyz"}`))
	req := httptest.NewRequest("PUT", "/api/echo", reqBody)
	res, err := app.Test(req)

	utils.AssertEqual(t, nil, err, "app.Test")
	utils.AssertEqual(t, 400, res.StatusCode, "HTTP Status Code")

	resBody, _ := io.ReadAll(res.Body)
	utils.AssertEqual(t, `Bad Request`, string(resBody), "Response Body")
}

// Test PUT when "echoed" is true
func TestEcho_PUT_echoed_true(t *testing.T) {
	app := fiber.New()
	app.Put("/api/echo", echo)

	reqBody := bytes.NewBuffer([]byte(`{"username":"xyz","upload":"xyz","echoed":true}`))
	req := httptest.NewRequest("PUT", "/api/echo", reqBody)
	res, err := app.Test(req)

	utils.AssertEqual(t, nil, err, "app.Test")
	utils.AssertEqual(t, 400, res.StatusCode, "HTTP Status Code")

	resBody, _ := io.ReadAll(res.Body)
	utils.AssertEqual(t, `Bad Request`, string(resBody), "Response Body")
}

// Test PUT when "echoed" is "true"
func TestEcho_PUT_echoed_notsotrue(t *testing.T) {
	app := fiber.New()
	app.Put("/api/echo", echo)

	reqBody := bytes.NewBuffer([]byte(`{"username":"xyz","upload":"xyz","echoed":"true"}`))
	req := httptest.NewRequest("PUT", "/api/echo", reqBody)
	res, err := app.Test(req)

	utils.AssertEqual(t, nil, err, "app.Test")
	utils.AssertEqual(t, 200, res.StatusCode, "HTTP Status Code")

	resBody, _ := io.ReadAll(res.Body)
	utils.AssertEqual(t, `{"echoed":true,"upload":"xyz","username":"xyz"}`, string(resBody), "Response Body")
}

// Test PUT when "echoed" is false
func TestEcho_PUT_echoed_false(t *testing.T) {
	app := fiber.New()
	app.Put("/api/echo", echo)

	reqBody := bytes.NewBuffer([]byte(`{"username":"xyz","upload":"xyz","echoed":false}`))
	req := httptest.NewRequest("PUT", "/api/echo", reqBody)
	res, err := app.Test(req)

	utils.AssertEqual(t, nil, err, "app.Test")
	utils.AssertEqual(t, 200, res.StatusCode, "HTTP Status Code")

	resBody, _ := io.ReadAll(res.Body)
	utils.AssertEqual(t, `{"echoed":true,"upload":"xyz","username":"xyz"}`, string(resBody), "Response Body")
}

// Test PUT when "echoed" is "false"
func TestEcho_PUT_echoed_notsofalse(t *testing.T) {
	app := fiber.New()
	app.Put("/api/echo", echo)

	reqBody := bytes.NewBuffer([]byte(`{"username":"xyz","upload":"xyz","echoed":"false"}`))
	req := httptest.NewRequest("PUT", "/api/echo", reqBody)
	res, err := app.Test(req)

	utils.AssertEqual(t, nil, err, "app.Test")
	utils.AssertEqual(t, 200, res.StatusCode, "HTTP Status Code")

	resBody, _ := io.ReadAll(res.Body)
	utils.AssertEqual(t, `{"echoed":true,"upload":"xyz","username":"xyz"}`, string(resBody), "Response Body")
}